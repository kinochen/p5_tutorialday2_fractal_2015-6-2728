
//float branchAngleA = -PI/4;
float branchAngleA = random(-PI/2);


void setup(){

  size(800,800);
  
}

void draw(){
  background(0);
   tree( width/2 -75, height, 150, 0 ,5);
//  tree( width/2 -75, height, 150, 0, (frameCount/10)%10);
}

void tree(float x, float y, float size, float angle, int limit ){
    //base
    pushMatrix();
      translate(x,y);  
      rotate(angle);
      fill(255-limit*20);
      rect(0, 0,size, -size); 

    //left side
        float x0 =0, 
              y0 = -size,
              size0 = abs( cos(branchAngleA)*size ), 
              angle0 = branchAngleA;
    
        if(limit >0){
          tree(x0, y0, size0, angle0, limit-1 );
        }else{
          pushMatrix();
            translate(x0,y0);  
            rotate(angle0);
            fill(0,255,0);
            rect(0, 0,size0, -size0);
          popMatrix();
        }
  //right side
        float x1 = x0 + cos(angle0) *size0,
              y1 = y0 + sin(angle0) *size0,
              size1 = abs( sin(branchAngleA) * size ),
              angle1 = angle0 + PI/2;
        if(limit >0){
          tree(x1, y1, size1, angle1, limit-1 );
        }else{
          pushMatrix();
             translate(x1,y1);  
             rotate(angle1);
             fill(255,0,0);
             rect(0, 0,size1, -size1);
          popMatrix();
        }
  popMatrix();
}
