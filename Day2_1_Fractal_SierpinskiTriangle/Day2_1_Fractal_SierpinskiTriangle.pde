
PVector t0, t1,t2;
int limit = 5;

void setup(){
   size(400,400);
   
    t0 = new PVector (200, 100);
    t1 = new PVector (350,346);//right corner
    t2 = new PVector (50,346); //left corner 


}

void draw(){
  background(0); 
 // triangle(t0.x, t0.y,t1.x, t1.y, t2.x, t2.y);
  sierpinski(t0, t1, t2, int(mouseX/50));
  
}

void drawTriangle(PVector p0, PVector p1, PVector p2){
  triangle(p0.x, p0.y, p1.x, p1.y, p2.x, p2.y);
//  beginShape();
//    vertex(p0.x, p0.y);
//    vertex(p1.x, p1.y);
//    vertex(p2.x, p2.y);
//    vertex(p0.x, p0.y);   
//  endShape();
  fill(30);
  stroke(255);
}

void sierpinski(PVector p0, PVector p1, PVector p2, int limit){
  
  if(limit >0){
   PVector pA = new PVector ((p0.x+p1.x)/2, (p0.y+p1.y)/2 );
   PVector pB = new PVector ((p1.x+p2.x)/2, (p1.y+p2.y)/2 );
   PVector pC = new PVector ((p2.x+p0.x)/2, (p2.y+p0.y)/2 );

// drawTriangle(pA, pB, pC);
   sierpinski(p0, pA, pC, limit-1);
   sierpinski(pA, p1, pB, limit-1);
   sierpinski(pC, pB, p2, limit-1);
   
  }else{
    drawTriangle(p0, p1, p2);
  }
   
   
}
