

PVector k0, k1,k2;

void setup(){
  size(400,400);
  k0 = new PVector(200,100);
  k1 = new PVector(350,346);
  k2 = new PVector(50,346);
}

void draw(){
background(0);
   koch(k2, k1, mouseX/50);
     stroke( 255);
//    
//    koch(k2, k1, 3);
//      stroke( 0,255,0 ,128 );
//    koch(k1, k0, 3);
//      stroke( 0,0,255 ,128 );
//    koch(k0, k2, 3);
//      stroke( 255,0,0,128 );



}

void koch(PVector k0, PVector k1, int limit){
  float  dx  = k1.x-k0.x;
  float  dy  = k1.y-k0.y;
  float  dist = sqrt(dx*dx + dy*dy);
  float  unit = dist/3;
  float  angle = atan2(dy, dx);  //
 // println(angle/(2*PI)*360);

  PVector pA = new PVector( k0.x + dx/3, k0.y+dy/3); 

  PVector pB = new PVector( pA.x + cos(angle - PI/3 )* unit,
                            pA.y + sin(angle - PI/3 )* unit); 

  PVector pC = new PVector( k1.x - dx/3, k1.y-dy/3); 
    
  if(limit > 0){
    koch(k0,pA, limit -1);
    koch(pA,pB, limit -1);
    koch(pB,pC, limit -1);
    koch(pC,k1, limit -1);
    
//    line(pA.x, pA.y,pB.x,pB.y);
//    line(pB.x, pB.y,pC.x,pC.y);
//     stroke( 32*(limit+1) );
  }else{
//    beginShape();
//      vertex(k0.x,k0.y);
//      vertex(pA.x,pA.y); 
//      vertex(pB.x,pB.y); 
//      vertex(pC.x,pC.y);
//      vertex(k1.x,k1.y);
//    endShape();
  line(k0.x, k0.y,pA.x,pA.y);
  line(pA.x, pA.y, pB.x,pB.y);
  line(pB.x, pB.y, pC.x,pC.y);
  line(pC.x, pC.y,k1.x,k1.y);

//    point(pA.x, pA.y);
//    point(pB.x, pB.y);
//    point(pC.x, pC.y);
   }
}
