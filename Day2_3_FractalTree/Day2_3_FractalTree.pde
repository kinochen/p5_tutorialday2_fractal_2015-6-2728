
PVector p0, p1;
float branchAngleA=random(-PI/2, PI/2);
float branchAngleB=random(-PI/2, PI/2);
float branchAngleAbase=random(-PI/2, PI/2);
float branchAngleBbase=random(-PI/2, PI/2);
float thetaA = 0;
float trunkRatio = 0.1;//random(.15, .75);    //?
void setup() {
  size(400, 400);

  p0 = new PVector(width/2, height-20);
  p1 = new PVector(width/2, 20);
}

void draw() {
  background(0);
  tree(p0, p1, mouseX/50);

  branchAngleA = branchAngleAbase * sin(thetaA);
  branchAngleB = branchAngleBbase * sin(thetaA);

  thetaA += 0.01;
  stroke(255);
}

void tree(PVector p0, PVector p1, int limit) {
  float dx = p1.x - p0.x;
  float dy = p1.y - p0.y;
  float dist = sqrt(dx*dx + dy*dy);
  float angle = atan2(dy, dx);
  float branchLength = dist * (1 - trunkRatio);

  PVector pA = new PVector(p0.x + dx * trunkRatio, p0.y +dy*trunkRatio);
  PVector pB = new PVector(pA.x + cos(angle + branchAngleA)* branchLength, 
  pA.y + sin(angle + branchAngleA)* branchLength   );
  PVector pC = new PVector(pA.x + cos(angle - branchAngleB)* branchLength, 
  pA.y + sin(angle - branchAngleB)* branchLength   );

  line(p0.x, p0.y, pA.x, pA.y);

  if (limit > 0) {
    tree(pA, pC, limit - 1);
    tree(pA, pB, limit - 1);
  } else {
    line(pB.x, pB.y, pA.x, pA.y);
    line(pA.x, pA.y, pC.x, pC.y);
  }
  //      branchAngleA += mouseY/8000.0;
  //      branchAngleB += mouseY/8000.0;
  //      trunkRatio = mouseY/400.0;
}

